package software.m.m.picturechatwierd;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mac on 24.12.16.
 */

public class ContactsResource extends Provider<List<Contact>,Void> {

    public void pull(Void v) {

        set(new ArrayList<Contact>(){
            {
                add(new Contact("Миша"));
                add(new Contact("Jack Harrer"));
            }
        });
    }
}
