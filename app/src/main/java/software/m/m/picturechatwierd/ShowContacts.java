package software.m.m.picturechatwierd;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mac on 24.12.16.
 */

public class ShowContacts extends Provider<Void,Void> {

    final ContactsResource contactsResource;

    final ContactsViewResource contactsViewResource;

    public ShowContacts(ContactsResource contactsResource, ContactsViewResource contactsViewResource) {
        this.contactsResource = contactsResource;
        this.contactsViewResource = contactsViewResource;
    }

    @Override
    public void pull(Void v) {
        contactsResource.pull(null, this);
        contactsViewResource.pull(null,this);
    }

    @Override
    public void onChange() {
        if(contactsResource.checkIfNotNull()&& contactsViewResource.checkIfNotNull()){
            final List<Contact> contacts = contactsResource.get(null);
            final RecyclerView contactList = contactsViewResource.get(null);
            Log.i("AAAAAAAAAAAAAAAA","Checked, got and ready to mingle"+ contacts);
            contactList.setAdapter(new RecyclerView.Adapter<ContactViewHolder>(){
                @Override
                public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                    TextView textView = new TextView(contactList.getContext());
                    return new ContactViewHolder(textView);
                }

                @Override
                public void onBindViewHolder(ContactViewHolder holder, int position) {
                    holder.textView.setText(contacts.get(position).name);
                }

                @Override
                public int getItemCount() {
                    return contacts.size();
                }
            });
        }

    }

    private class ContactViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public ContactViewHolder(TextView itemView) {
            super(itemView);
            textView = itemView;
        }
    }
}
