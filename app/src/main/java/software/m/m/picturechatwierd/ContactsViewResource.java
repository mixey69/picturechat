package software.m.m.picturechatwierd;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;

/**
 * Created by mac on 24.12.16.
 */

public class ContactsViewResource extends Provider<RecyclerView,Void>{

    private final Activity hostActivity;

    public ContactsViewResource(Activity activity) {
        hostActivity = activity;
    }

    @Override
    void pull(Void request) {
        set((RecyclerView) hostActivity.findViewById(R.id.recycler_view));
    }
}
