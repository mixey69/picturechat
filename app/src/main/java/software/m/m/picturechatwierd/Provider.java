package software.m.m.picturechatwierd;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mac on 24.12.16.
 */

public class Provider<T,R> implements Callback {

    private T value;

    public Set<Callback> listeners = new HashSet<>();

    public void subscribe(Callback c){
        listeners.add(c);
    }

    public void unsubscribe(Callback c){
        listeners.remove(c);
    }

    public void addDependency(Provider r){
        r.subscribe(this);
    }

    public void removeDependency(Provider r){
        r.unsubscribe(this);
    }

    public void set(T t){
        value = t;
        for (Callback listener:listeners){
            listener.onChange();
        }
    }

    public T get(R request){
        return value;
    }

    void pull(R request){
    }

    public final void pull(R request, Callback dependent){
        subscribe(dependent);
        pull(request);
    }

    @Override
    public void onChange() {
    }

    public boolean checkIfNotNull(){
        return (value != null);
    }

}
